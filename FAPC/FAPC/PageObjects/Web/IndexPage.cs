﻿using OpenQA.Selenium;

namespace FAPC.PageObjects.Web
{
    public class IndexPage
    {
        public static By FaleConosco()
        {
            return By.XPath("//a[@title='Fale conosco']");
        }
    }
}