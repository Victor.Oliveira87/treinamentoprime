﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using AutoIt;
using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using RestSharp;

namespace FAPC
{
    public abstract class TestBase
    {
        #region Globals

        public enum DriverType { Web, Mobile, Desktop, Service };

        public string App = "calc.exe";
        public static string WinTitle;
        public static IntPtr WinHandle;

        public static DesiredCapabilities Dc = new DesiredCapabilities();

        public static bool IsDesktopApplication;
        public static bool FailIfNotExist = true;
        public const int DefaultTimedOut = 10;
        public static RemoteWebDriver Driver;
        public string Url { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public string ExtentFileName;
        public static string TestResultsDirectory;
        public static string EvidenceFileName;
        public static string TestInfo;
        public static string Logger = string.Empty;
        public string Description = string.Empty;
        public string Title = string.Empty;
        public ExtentReports Extent;
        public static ExtentTest Test;
        public static Screenshot Screenshot;
        public ExtentHtmlReporter HtmlReporter;
        public static string PlatformName;
        public static string PlatformVersion;
        public static string AppPackage;
        public static string AppActivity;
        public static string DeviceName;

        #endregion

        #region Attr

        [TestInitialize()]
        public void MyTestInitialize()
        {
            SetAmbiente();

            TestResultsDirectory = TestContext.TestResultsDirectory;

            ExtentFileName = Path.Combine(TestResultsDirectory, TestContext.TestName + '_' + DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".html");

            if (!Directory.Exists(TestResultsDirectory))
            {
                Directory.CreateDirectory(TestResultsDirectory);
            }

            if (!File.Exists(ExtentFileName))
            {
                File.Create(ExtentFileName);
            }

            HtmlReporter = new ExtentHtmlReporter(ExtentFileName);
            Extent = new ExtentReports();
            Extent.AttachReporter(HtmlReporter);

            Test = Extent.CreateTest($"{TestContext.FullyQualifiedTestClassName} {TestContext.TestName}" + " " + Title, Description);
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            if (TestContext.CurrentTestOutcome != UnitTestOutcome.Passed)
            {
                Test.Fail(GetErrorMessage());
            }

            try
            {
                Driver.Quit();
            }
            catch (Exception)
            {
                //to do
            }

            try
            {
                AutoItX.WinKill(WinHandle);
            }
            catch (Exception)
            {
                //to do
            }

            Extent.Flush();
            HtmlReporter.Stop();

            using (StreamWriter sw = new StreamWriter(ExtentFileName, true))
            {
                sw.WriteLine("<style>img {border: 1px solid #ddd;border-radius: 4px;padding: 5px;width: 150px;} img:hover {box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);}</style><script>function OpenImage(src){ var newTab = window.open(); newTab.document.body.innerHTML = " + '"' + "<img src=" + '"' + " + src + " + '"' + ">" + '"' + ";}</script>");
                sw.Close();
            }

            Extent = null;
            HtmlReporter = null;
            Test = null;
            Logger = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
            ExecuteCmd("taskkill /im chromedriver.exe /f /t");

            if (TestContext.CurrentTestOutcome.Equals(UnitTestOutcome.Passed))
            {
                string passedTestsDir =
                    Path.Combine(AppDomain.CurrentDomain.BaseDirectory.Split(new[] { "TestResults" }, StringSplitOptions.None).First(), "TestResults",
                        $"Deploy_{DateTime.Now:ddMMyyyyThhmmss}");
                string passedExtentFileName = Path.Combine(passedTestsDir, ExtentFileName.Split('\\').Last());

                if (!Directory.Exists(passedTestsDir))
                {
                    Directory.CreateDirectory(passedTestsDir);
                }

                File.Copy(ExtentFileName, Path.Combine(passedTestsDir, passedExtentFileName));

                ExtentFileName = passedExtentFileName;
            }

            TestContext.AddResultFile(ExtentFileName);
        }

        #endregion

        #region Properties

        public TestContext TestContext
        {
            get
            {
                return _testContextInstance;
            }
            set
            {
                _testContextInstance = value;
            }
        }

        private TestContext _testContextInstance;

        #endregion

        #region Methods

        public IRestResponse RunService<T>(string uRl, T model, Method method = Method.POST) where T : class
        {
            return GetResponse(uRl, JsonConvert.SerializeObject(model), method);
        }

        public IRestResponse GetResponse(string uRl, string requestBody, Method method)
        {
            IRestClient client = CreateClient(uRl);
            IRestRequest request = CreateRequest(method);
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", requestBody, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            return response;
        }

        public IRestClient CreateClient(string uRl)
        {
            return new RestClient(Url + uRl);
        }

        public IRestRequest CreateRequest(Method method)
        {
            RestRequest request = new RestRequest(method);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Ocp-Apim-Subscription-Key", Token);

            return request;
        }

        public void SetDriverType(DriverType typeDriver)
        {
            switch (typeDriver)
            {
                case DriverType.Service:
                    IsDesktopApplication = false;

                    try
                    {
                        AutoItX.WinKill(WinHandle);
                    }
                    catch (Exception)
                    {
                        //to do
                    }

                    try
                    {
                        Driver.Quit();
                    }
                    catch (Exception)
                    {
                        // to do
                    }

                    SetServiceConfig();

                    break;

                case DriverType.Web:
                    IsDesktopApplication = false;

                    try
                    {
                        AutoItX.WinKill(WinHandle);
                    }
                    catch (Exception)
                    {
                        //to do
                    }

                    try
                    {
                        Driver.Quit();
                        Driver = new ChromeDriver(Path.Combine(AppDomain.CurrentDomain.BaseDirectory.Split(new[] { "TestResults" }, StringSplitOptions.None).First(), Assembly.GetCallingAssembly().GetName().Name, "bin", "Debug", "Deploy"));
                    }
                    catch (Exception)
                    {
                        Driver = new ChromeDriver(Path.Combine(AppDomain.CurrentDomain.BaseDirectory.Split(new[] { "TestResults" }, StringSplitOptions.None).First(), Assembly.GetCallingAssembly().GetName().Name, "bin", "Debug", "Deploy"));
                    }

                    Driver.Manage().Window.Maximize();

                    SetWebConfig();

                    break;

                case DriverType.Mobile:
                    IsDesktopApplication = false;

                    try
                    {
                        AutoItX.WinKill(WinHandle);
                    }
                    catch (Exception)
                    {
                        //to do
                    }

                    try
                    {
                        Driver.Quit();
                        Driver = new AndroidDriver<AndroidElement>(new Uri("http://127.0.0.1:4723/wd/hub"), Dc);

                    }
                    catch (Exception)
                    {
                        Driver = new AndroidDriver<AndroidElement>(new Uri("http://127.0.0.1:4723/wd/hub"), Dc);
                    }

                    SetMobileConfig();

                    break;

                case DriverType.Desktop:
                    IsDesktopApplication = true;

                    string destinationFileName1 = Path.Combine(TestContext.DeploymentDirectory, "AutoItX3_x64.dll");
                    string destinationFileName2 = Path.Combine(TestContext.DeploymentDirectory, "AutoItX3.dll");

                    if (!File.Exists(destinationFileName1))
                    {
                        File.Copy(
                            Path.Combine(
                                AppDomain.CurrentDomain.BaseDirectory
                                    .Split(new[] { "TestResults" }, StringSplitOptions.None).First(),
                                Assembly.GetCallingAssembly().GetName().Name, "bin", "Debug", "Deploy",
                                "AutoItX3_x64.dll"), destinationFileName1);
                    }

                    if (!File.Exists(destinationFileName2))
                    {
                        File.Copy(
                            Path.Combine(
                                AppDomain.CurrentDomain.BaseDirectory
                                    .Split(new[] { "TestResults" }, StringSplitOptions.None).First(),
                                Assembly.GetCallingAssembly().GetName().Name, "bin", "Debug", "Deploy",
                                "AutoItX3.dll"), destinationFileName2);
                    }

                    AutoItX.Run(App, "");
                    AutoItX.WinWaitActive(WinTitle);
                    WinHandle = AutoItX.WinGetHandle(WinTitle);

                    SetDesktopConfig();

                    break;
            }
        }

        public void SetDesktopConfig()
        {
            string desktopConfigFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "DesktopConfig.txt");

            if (File.Exists(desktopConfigFile))
            {
                using (StreamReader sr = new StreamReader(desktopConfigFile))
                {
                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();

                        if (line != null && line.Contains("App:"))
                        {
                            App = line.Split(new[] { "App:" }, StringSplitOptions.None).Last().Trim();
                        }

                        if (line != null && line.Contains("WinTitle:"))
                        {
                            WinTitle = line.Split(new[] { "WinTitle:" }, StringSplitOptions.None).Last().Trim();
                        }
                    }
                }
            }
        }

        public void SetWebConfig()
        {
            string webConfigFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "WebConfig.txt");

            if (File.Exists(webConfigFile))
            {
                using (StreamReader sr = new StreamReader(webConfigFile))
                {
                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();

                        if (line != null && line.Contains("Url:"))
                        {
                            Url = line.Split(new[] { "Url:" }, StringSplitOptions.None).Last().Trim();
                        }

                        if (line != null && line.Contains("User:"))
                        {
                            User = line.Split(new[] { "User:" }, StringSplitOptions.None).Last().Trim();
                        }

                        if (line != null && line.Contains("Password:"))
                        {
                            Password = line.Split(new[] { "Password:" }, StringSplitOptions.None).Last().Trim();
                        }
                    }
                }
            }
        }

        public void SetMobileConfig()
        {
            string mobileConfigFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "MobileConfig.txt");

            if (File.Exists(mobileConfigFile))
            {
                using (StreamReader sr = new StreamReader(mobileConfigFile))
                {
                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();
                        if (line != null) Dc.SetCapability(line.Split(':').First(), line.Split(':').Last());
                    }
                }
            }
        }

        public void SetServiceConfig()
        {
            string serviceCombine = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ServiceConfig.txt");

            if (File.Exists(serviceCombine))
            {
                using (StreamReader sr = new StreamReader(serviceCombine))
                {
                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();

                        if (line != null && line.Contains("Url:"))
                        {
                            Url = line.Split(new[] { "Url:" }, StringSplitOptions.None).Last().Trim();
                        }

                        if (line != null && line.Contains("User:"))
                        {
                            User = line.Split(new[] { "User:" }, StringSplitOptions.None).Last().Trim();
                        }

                        if (line != null && line.Contains("Password:"))
                        {
                            Password = line.Split(new[] { "Password:" }, StringSplitOptions.None).Last().Trim();
                        }

                        if (line != null && line.Contains("Token:"))
                        {
                            Token = line.Split(new[] { "Token:" }, StringSplitOptions.None).Last().Trim();
                        }
                    }
                }
            }
        }

        public List<List<KeyValuePair<string, string>>> ExecuteQuery(string sqlScript, Dictionary<string, string> param = null)
        {
            List<KeyValuePair<string, string>> lstResultado;
            List<List<KeyValuePair<string, string>>> lstResultados = new List<List<KeyValuePair<string, string>>>();
            string connectionString, query;
            connectionString = "Data Source=10.140.10.11;Initial Catalog='PARCDCEMPR';Integrated Security=True;Persist Security Info=False;Pooling=False;MultipleActiveResultSets=False;Encrypt=False;TrustServerCertificate=True";
            for (int i = 0; i < 3; i++)
            {
                lstResultados = new List<List<KeyValuePair<string, string>>>();

                try
                {
                    if (!sqlScript.Contains(".sql"))
                    {
                        sqlScript = sqlScript + ".sql";
                    }
                    string fileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data", sqlScript);

                    query = File.ReadAllText(fileName);
                    if (param != null)
                    {
                        foreach (string k in param.Keys)
                        {
                            string keyName = "@" + k;
                            query = query.Replace(keyName, param[k]);
                        }
                    }
                    SqlConnection sqlConnection = new SqlConnection(connectionString);
                    sqlConnection.Open();
                    SqlCommand cmd = new SqlCommand(query, sqlConnection);
                    SqlDataReader dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        lstResultado = new List<KeyValuePair<string, string>>();

                        for (int j = 0; j < dr.FieldCount; j++)
                        {
                            lstResultado.Add(new KeyValuePair<string, string>(dr.GetName(j), dr[j].ToString()));
                        }

                        lstResultados.Add(lstResultado);
                    }
                    dr.Close();
                    cmd.Dispose();
                    sqlConnection.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message + " " + e.InnerException);
                }
                if (lstResultados.Count > 0)
                {
                    break;
                }
            }

            return lstResultados;
        }

        public void SetAmbiente()
        {
            ExecuteCmd($"taskkill /im {App} /f /t");
            ExecuteCmd("taskkill /im chromedriver.exe /f /t");
            ExecuteCmd("taskkill /im chrome.exe /f /t");

            SetDesktopConfig();
            SetMobileConfig();
            SetWebConfig();
            SetServiceConfig();
        }

        public static string ConvertImageToBase64(string fileName)
        {
            byte[] imageArray = File.ReadAllBytes(fileName);
            return Convert.ToBase64String(imageArray);
        }

        public static void Checkpoint(bool condition, string message, bool takeScreenShot = true)
        {
            if (takeScreenShot)
            {
                if (IsDesktopApplication)
                {
                    var size = AutoItX.WinGetPos(WinHandle);

                    Bitmap screenshot = new Bitmap(size.Width, size.Height);

                    Graphics graphics = Graphics.FromImage(screenshot);

                    graphics.CopyFromScreen(size.X, size.Y, 0, 0, screenshot.Size);

                    EvidenceFileName = Path.Combine(TestResultsDirectory, "evidence" + DateTime.Now.ToString("ddMMyyyyThhmmss") + ".png");

                    screenshot.Save(EvidenceFileName, ImageFormat.Png);
                }
                else
                {
                    Screenshot = ((ITakesScreenshot)Driver).GetScreenshot();

                    EvidenceFileName = Path.Combine(TestResultsDirectory,
                        "evidence" + DateTime.Now.ToString("ddMMyyyyThhmmss") + ".png");

                    Screenshot.SaveAsFile(EvidenceFileName, ScreenshotImageFormat.Png);
                }

                TestInfo = "<h5>" + Logger + "</h5><h10>" + message + "</h10><br/><a target='_blank' onclick=OpenImage('data:image/png;base64," + ConvertImageToBase64(EvidenceFileName) + "')><img src='data:image/png;base64," + ConvertImageToBase64(EvidenceFileName) + "' alt='Forest'></a>";

                File.Delete(EvidenceFileName);
            }
            else
            {
                TestInfo = "<h5>" + Logger + "</h5><h10>" + message + "</h10>";
            }

            if (condition)
            {
                Test.Pass(TestInfo);
            }
            else
            {
                Test.Fail(TestInfo);

                Assert.Fail(message);
            }
        }

        public string GetErrorMessage()
        {
            const BindingFlags privateGetterFlags = BindingFlags.GetField |
                                                    BindingFlags.GetProperty |
                                                    BindingFlags.NonPublic |
                                                    BindingFlags.Instance |
                                                    BindingFlags.FlattenHierarchy;

            var mMessage = string.Empty; // Returns empty if TestOutcome is not failed
            if (TestContext.CurrentTestOutcome == UnitTestOutcome.Failed)
            {
                // Get hold of TestContext.m_currentResult.m_errorInfo.m_stackTrace (contains the stack trace details from log)
                var field = TestContext.GetType().GetField("m_currentResult", privateGetterFlags);
                if (field != null)
                {
                    var mCurrentResult = field.GetValue(TestContext);
                    field = mCurrentResult.GetType().GetField("m_errorInfo", privateGetterFlags);
                    if (field != null)
                    {
                        var mErrorInfo = field.GetValue(mCurrentResult);
                        field = mErrorInfo.GetType().GetField("m_stackTrace", privateGetterFlags);
                        if (field != null) mMessage = field.GetValue(mErrorInfo) as string;
                    }
                }
            }

            return mMessage;
        }

        public List<KeyValuePair<string, string>> ExecuteCmd(string command)
        {
            Process process = new Process();
            List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();

            process.StartInfo.FileName = "cmd.exe";
            process.StartInfo.Arguments = "/C " + command;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.Start();
            process.WaitForExit();

            list.Add(new KeyValuePair<string, string>("Output", process.StandardOutput.ReadToEnd()));
            list.Add(new KeyValuePair<string, string>("Error", process.StandardError.ReadToEnd()));

            Thread.Sleep(3000);

            return list;
        }

        #endregion
    }

    public static class Util
    {
        public static FwControl Click(this FwControl control, int timeout = TestBase.DefaultTimedOut)
        {
            int done = 0;

            for (int i = 0; i < timeout; i++)
            {
                AutoItX.WinWaitActive(control.Title, "", timeout);

                if (!string.IsNullOrEmpty(control.ClassNameNn))
                {
                    done = AutoItX.ControlClick(control.Title, "", control.ClassNameNn);
                }

                if (done.Equals(1))
                {
                    TestBase.Checkpoint(true, $"Efetuar Click no Elemento: {control.ClassNameNn}");

                    return control;
                }
                else
                {
                    Thread.Sleep(1000);
                }
            }

            if (!done.Equals(1))
            {
                TestBase.Checkpoint(false, $"Não foi possível encontrar o Elemento: {control.ClassNameNn}");
            }

            return control;
        }

        public static FwControl SendKeys(this FwControl control, string text, int timeout = TestBase.DefaultTimedOut)
        {
            int done = 0;

            for (int i = 0; i < timeout; i++)
            {
                AutoItX.WinWaitActive(control.Title, "", timeout);

                if (!string.IsNullOrEmpty(control.ClassNameNn))
                {
                    done = AutoItX.ControlSend(control.Title, "", control.ClassNameNn, text);
                }

                if (done.Equals(1))
                {
                    TestBase.Checkpoint(true, $"Enviar texto: ({text}) no Elemento: {control.ClassNameNn}");

                    return control;
                }
                else
                {
                    Thread.Sleep(1000);
                }
            }

            if (!done.Equals(1))
            {
                TestBase.Checkpoint(false, $"Não foi possível encontrar o Elemento: {control.ClassNameNn}");
            }

            return control;
        }

        public static bool CheckPoint(this By by, int timeoutSeconds = TestBase.DefaultTimedOut)
        {
            for (int i = 0; i < timeoutSeconds; i++)
            {
                try
                {
                    if (TestBase.Driver.FindElement(by).Displayed)
                    {
                        TestBase.Checkpoint(true, $"Verificar se existe o Elemento de Localizador: ({by})");

                        return true;
                    }
                }
                catch (Exception)
                {
                    Thread.Sleep(1000);
                }
            }

            if (TestBase.FailIfNotExist)
            {
                try
                {
                    if (TestBase.Driver.FindElement(by).Displayed)
                    {
                        TestBase.Checkpoint(true, $"Verificar se existe o Elemento de Localizador: ({by})");

                        return true;
                    }
                }
                catch (Exception)
                {
                    TestBase.Checkpoint(false, $"Não foi possível encontrar o Elemento pelo Localizador: ({by})");
                }
            }

            return false;
        }

        public static By Click(this By by, int timeoutSeconds = TestBase.DefaultTimedOut)
        {
            for (int i = 0; i < timeoutSeconds; i++)
            {
                try
                {
                    TestBase.Driver.FindElement(by).Click();
                    TestBase.Checkpoint(true, $"Efetuar Click no Elemento de Localizador: ({by})");

                    return by;
                }
                catch (Exception)
                {
                    Thread.Sleep(1000);
                }
            }

            if (TestBase.FailIfNotExist)
            {
                try
                {
                    TestBase.Driver.FindElement(by).Click();
                    TestBase.Checkpoint(true, $"Efetuar Click no Elemento de Localizador: ({by})");
                }
                catch (Exception)
                {
                    TestBase.Checkpoint(false, $"Não foi possível encontrar o Elemento pelo Localizador: ({by})");
                }
            }

            return by;
        }

        public static By SendKeys(this By by, string text, int timeoutSeconds = TestBase.DefaultTimedOut)
        {
            for (int i = 0; i < timeoutSeconds; i++)
            {
                try
                {
                    TestBase.Driver.FindElement(by).SendKeys(text);
                    TestBase.Checkpoint(true, $"Enviar texto: ({text}) no Elemento de Localizador: ({by})");

                    return by;
                }
                catch (Exception)
                {
                    Thread.Sleep(1000);
                }
            }

            if (TestBase.FailIfNotExist)
            {
                try
                {
                    TestBase.Driver.FindElement(by).SendKeys(text);
                    TestBase.Checkpoint(true, $"Enviar texto: ({text}) no Elemento de Localizador: ({by})");
                }
                catch (Exception)
                {
                    TestBase.Checkpoint(false, $"Não foi possível encontrar o Elemento pelo Localizador: ({by})");
                }
            }

            return by;
        }
    }

    public class FwControl : TestBase
    {
        private string _title;

        public FwControl(string title)
        {
            _title = title;
        }

        public new string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public string Class { get; set; }
        public string Instance { get; set; }
        public string ClassNameNn { get; set; }
        public string Name { get; set; }
        public string AdvancedMode { get; set; }
        public int Id { get; set; }
        public KeyValuePair<int, int> Position { get; set; }
        public string Text { get; set; }
        public KeyValuePair<int, int> Size { get; set; }
        public KeyValuePair<int, int> ControlClickCoords { get; set; }
        public string Style { get; set; }
        public string ExStyle { get; set; }
        public string Handle { get; set; }
    }
}