﻿using FAPC.PageObjects.Web;
using OpenQA.Selenium;

namespace FAPC.Controller
{
    public class IndexController : TestBase
    {
        public static By ClickFaleConosco(int timeout = DefaultTimedOut)
        {
            return IndexPage.FaleConosco().Click(timeout);
        }
    }
}